import React from 'react'
import { Text, View } from 'react-native'

export default function Feed() {
    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text>Feed screen</Text>
        </View>
    )
}
