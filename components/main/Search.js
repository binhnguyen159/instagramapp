import React, { useState } from 'react'
import { Text, View } from 'react-native'
import firebase from 'firebase'
import 'firebase/firestore';
import { FlatList, TextInput, TouchableOpacity } from 'react-native-gesture-handler';



export default function Search(props) {
    const [users, setUsers] = useState([]);
    const fetchUsers = (search) => {
        firebase.firestore()
            .collection('users')
            .where(search, '<=', 'name')
            .get()
            .then((snapshot) => {
                let users = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data }
                });
                setUsers(users)
            })
    }
    return (
        <View>

            <TextInput
                placeholder="Type here..."
                onChangeText={(search) => fetchUsers(search)} />

            <FlatList
                numColumns={1}
                horizontal={false}
                data={users}
                renderItem={({ item }) => (
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate("Profile", { uid: item.id })} >
                        <Text>{item.name}</Text>
                    </TouchableOpacity>
                )} />
        </View>
    )
}