import firebase from 'firebase';
import React, { Component } from 'react'
import { Button, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'

export class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
        }
        this.onLogIn = this.onLogIn.bind(this);
    }

    onLogIn() {
        const { email, password } = this.state;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(result => console.log(result))
            .catch(err => console.log(err));
    }

    render() {
        return (
            <View>
                <TextInput
                    placeholder="email"
                    onChangeText={(email) => this.setState({ email })} />
                <TextInput
                    placeholder="password"
                    onChangeText={(password) => this.setState({ password })}
                    secureTextEntry={true} />
                <Button
                    title="Log in"
                    onPress={() => this.onLogIn()} />
            </View>
        )
    }
}

export default Login
